package main

import "fmt"

func main() {
	fmt.Printf("Content-type: text/plain\n\n")
	fmt.Printf(`Hello from Golang

source: https://gitlab.com/ariews/go.cgi`)
}
